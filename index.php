<?php
require_once "class/article.php";
require_once "class/database.php";
require_once "class/post.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Blog</title>
</head>

<body>

    <div class="blog-container">
        <div class="blog-post">
            <h2 class="blog-title">My Blog Post</h2>
            <p class="blog-author">By John Doe</p>
            <div class="blog-content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
            </div>
            <div class="article">
                <?php
               $article = new Article();
               $result = $article->readOne($id);
               
               echo '<h2>' . $result['title'] . '</h2>';
               echo '<p>' . $result['content'] . '</p>';
               echo '<p>' . $result['info'] . '</p>';
               echo '<p>' . date('d-m-Y H:i:s', strtotime($result['date'])) . '</p>';
               

                ?>
                <h3 class="article-title">Article Title</h3>
                <p class="article-author">By Jane Doe</p>
                <div class="article-content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.
                </div>
            </div>
        </div>
        <div class="comments">
            <h3 class="comments-title">Comments</h3>
            <div class="comment">
                <p class="comment-author">Jane Doe</p>
                <!-- <div class=" -->

</body>

</html>