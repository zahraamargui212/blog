<?php
class Article
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $this->conn = $database->getConnection();
    }

    public function create($post_id, $title, $author, $content, $info)
    {
        $query = "INSERT INTO articles SET post_id=:post_id, title=:title, author=:author, content=:content, info=:info";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":post_id", $post_id);
        $stmt->bindParam(":title", $title);
        $stmt->bindParam(":author", $author);
        $stmt->bindParam(":content", $content);
        $stmt->bindParam(":info", $info);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    public function readOne($id)
    {
        $query = "SELECT * FROM articles WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

}
