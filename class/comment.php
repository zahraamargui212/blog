<?php

class Comment
{
    private $conn;

    public function __construct()
    {
        $database = new Database();
        $this->conn = $database->getConnection();
    }

    public function create($post_id, $author, $content)
    {
        $query = "INSERT INTO comments SET post_id=:post_id, author=:author, content=:content";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":post_id", $post_id);
        $stmt->bindParam(":author", $author);
        $stmt->bindParam(":content", $content);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function read($post_id)
    {
        $query = "SELECT * FROM comments WHERE post_id = :post_id ORDER BY id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":post_id", $post_id);
        $stmt->execute();
        return $stmt;
    }

    public function update($id, $content)
    {
        $query = "UPDATE comments SET content=:content WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":content", $content);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function delete($id)
    {
        $query = "DELETE FROM comments WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":id", $id);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
}
